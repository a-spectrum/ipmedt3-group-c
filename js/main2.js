/*jshint esversion: 6 */
window.onload = () => {
    let textAudioIsFinished = true;
    let dialogAudio = new Audio("");
    const invisiblePlaceholderText = document.getElementById("js--invisiblePlaceholderText");
    let dialogObject = {
        1: {
            "text": "Hey jij! Ik ben jouw BHV-buddy en ga jou leren 'Hoe te handelen bij een brand'. Dit doe je op de 7de verdieping van het Carrefour gebouw waar de Hogeschool Leiden zich bevindt. Als je klaar bent voor de volgende stap in deze belevenis, kijk je naar de afspeel-knop.",
            "audio": "media/dialog/audio1.mp3",
            "time": 14000
        },
        2: {
            "text": "Een brand kan pas ontstaan als drie factoren tegelijk aanwezig zijn. Deze drie vormen samen de branddriehoek. Deze bestaat uit de volgende 3 zijden: Temperatuur, Brandstof en Zuurstof. Laten we ze even met je doornemen.",
            "audio": "media/dialog/audio2.mp3", "time": 14000
        },
        3: {
            "text": "De temperatuur. Door de ontbrandings- temperatuur weg te nemen kan een brand worden gestopt of vermindert. Bijvoorbeeld met een blusmiddel zoals water. Hiermee kan de temperatuur worden verlaagd en zal er niet voldoende ontsteking meer zijn. De brand zal doven.",
            "audio": "media/dialog/audio3.mp3", "time": 15000
        },
        4: {
            "text": "De brandstof. Door de brandstof weg te halen of te isoleren kan een brand worden gestopt of vermindert. Bijvoorbeeld door het dichtdraaien van de gaskraan.",
            "audio": "media/dialog/audio4.mp3", "time": 8000
        },
        5: {
            "text": "De zuurstof. Door de aanwezigheid van zuurstof te verhinderen kan een brand worden gestopt of vermindert. Bijvoorbeeld met behulp van een branddeken om de zuurstof- toevoer af te sluiten. Het sluiten van ramen en deuren verhindert de toevoer van zuurstof ook.",
            "audio": "media/dialog/audio5.mp3", "time": 14000
        },
        6: {
            "text": "Bij een brand in dit gebouw is het van belang dat jij als toekomstig BHV'er gelijk weet te handelen. Stap 1 is in contact komen met de BHV-coordinator. Hij geeft je de instructies die je moet ondernemen. Ook ontvang je van hem een geel bhv-hesje.",
            "audio": "media/dialog/audio6.mp3", "time": 15000
        },
        7: {
            "text": "O NEE HE... HET BRAND ALARM GAAT AF. Dit kan geen toeval zijn. Laat mij jouw als BHV-coordinator helpen de instructies te geven om deze brand zo snel mogelijk te stoppen. Ga snel naar de locatie van de brand door de groene cilinders te volgen.",
            "audio": "media/dialog/audio7.mp3", "time": 15000
        },
        8: {
            "text": "Je bevindt je nu op de plaats waar de brandblusser te vinden is. Omdat je nog niet weet of je de brand zelf moet blussen, zal je eerst opzoek moeten gaan naar de brand om de situatie in te schatten. Vervolg je weg naar het zoeken van de brand.",
            "audio": "media/dialog/audio8.mp3", "time": 12000
        },
        9: {
            "text": "Je hebt de brand gevonden! Door het raam is te zien dat er een laptop in de brand staat achter de gesloten deur. Je zult de situatie nu eerst verder moeten inschatten om te kunnen bepalen of de brand zelf kan worden geblust. Laten we het stappenplan hiervoor onder de loep nemen.",
            "audio": "media/dialog/audio9.mp3", "time": 14000
        },
        10: {
            "text": "Door met de rug van je hand de kieren van de deur en de deurklink te controleren, check je of er ergens warmte vandaan komt.",
            "audio": "media/dialog/audio10.mp3", "time": 7000
        },
        11: {
            "text": "Voel je warme lucht door de kieren heen of is de deurklink heet, laat de deur dan dicht en schakel gelijk de brandweer in. In dit geval is dat niet zo en vervolg je het stappenplan. Blijf laag en ga gehurkt zitten met je hoofd afgewend van de deur. Open de deur op een kier.",
            "audio": "media/dialog/audio11.mp3", "time": 16000
        },
        12: {
            "text": "Wacht 5 seconden en bekijk of er rook achter de deur vandaan komt. Zie je rook, sluit dan gelijk de deur en schakel ook nu gelijk de brandweer in. Ook dit is nu niet het geval. Je kunt de brand dus zelf gaan blussen. Sluit de deur en haast je naar de schuimblusser toe.",
            "audio": "media/dialog/audio12.mp3", "time": 15000
        },
        13: {
            "text": "Op elke verdieping van de Hogeschool Leiden in het Carrefour gebouw bevindt zich een schuimblusser. Bij een schuimblusser komen water en schuim als druppeltjes uit het spuitmondje. Hierdoor bestaat er geen gevaar voor elektrocutie en is deze geschikt voor het blussen van de laptop brand.",
            "audio": "media/dialog/audio13.mp3", "time": 14000
        },
        14: {
            "text": "Pak de brandblusser en ga snel terug om de brand te blussen.",
            "audio": "media/dialog/audio14.mp3", "time": 3000
        },
        15: {
            "text": "Ga snel naar binnen om de brand te blussen.",
            "audio": "media/dialog/audio15.mp3", "time": 2000
        },
        16: {
            "text": "We gaan nu de schuimblusser gebruiken om de brand te blussen. Verwijder de borgpen en geef een proefstoot.",
            "audio": "media/dialog/audio16.mp3", "time": 5000
        },
        17: {
            "text": "Houdt een veilige afstand aan. Spuit zo laag mogelijk en onafgebroken door het handvat ingedrukt te houden. Ga door tot de brand gedoofd is en check of de brand goed geblust is.",
            "audio": "media/dialog/audio17.mp3", "time": 10000
        },
        18: {
            "text": "Het is je gelukt! De brand is geblust. Let op dat je na het blussen van de brand ook altijd het alarmnummer belt. Deze is als volgt: 0655429118. Deze kun je ook terug vinden op de pas die elke medewerker van de Hogeschool Leiden bij zich moet hebben.",
            "audio": "media/dialog/audio18.mp3", "time": 18000
        },
        19: {
            "text": "Loop naar buiten om je certificaat in ontvangst te nemen.",
            "audio": "media/dialog/audio19.mp3", "time": 2000
        },
        20: {
            "text": "Gefeliciteerd met het voltooien van de taak “Het handelen bij een brand”. Wist jij dat de brandweer alleen al in Zuid-Holland zeer vaak moet uitrukken? Alleen daarom is het al belangrijk om te weten hoe je handelt bij een brand, in het geval dat jij ooit in zo´n situatie belandt.",
            "audio": "media/dialog/audio20.mp3", "time": 14000
        },
        21: {
            "text": "Dit is het einde van de VR-beleving, je kan nog een kijk nemen bij de jaartallen om te zien hoe vaak ze in dat betreffende jaar moesten uitrukken. Daarna kun je de VR-bril afzetten. Dank u wel voor het doorlopen van deze VR-beleving.",
            "audio": "media/dialog/audio21.mp3", "time": 9000
        },
    };

    const dialogGetTime = (key) => {
        return dialogObject[key]["time"] + 500;
    };

    const dialogPlayAudioAndSetText = (key, textElement) => {
        if (textAudioIsFinished) {
            textAudioIsFinished = false;
            textElement.setAttribute("value", dialogObject[key]["text"]);
            dialogAudio.src = dialogObject[key]["audio"];
            dialogAudio.play();

            dialogAudio.onloadedmetadata = function () {
                setTimeout(function () {
                    textAudioIsFinished = true;
                }, String(dialogAudio.duration).split(".")[0] + "000");
            };
        }
    };

    const delay = (milliseconds) => {
        return new Promise(resolve => setTimeout(resolve, milliseconds));
    };

    const togglePlayIconVisibility = () => {
        if (playIconVisible) {
            playIcon.setAttribute("position", "-0.5 1.7 0");
            playIconVisible = false;
        } else {
            playIcon.setAttribute("position", "0.28 1.7 0");
            playIconVisible = true;
        }
    };

    const toggleClickable = (element) => {
        if (element.classList.contains("clickable")) {
            element.classList.remove("clickable");
        }
        else {
            element.classList.add("clickable");
        }
    };

    let speed = 40;
    let talk;
    let booleaan = false;
    // let hold = null;
    let lastText = true;

    let openDeur = false;

    let playIconVisible = true;
    let blusserInHanden = false;

    let act8HasntPlayed = true;
    let act9HasntPlayed = true;
    let act13HasntPlayed = true;
    let act15HasntPlayed = true;
    let act16HasntPlayed = true;
    let act20HasntPlayed = true;

    let branddriehoekZuurstofRead = false;
    let branddriehoekBrandstofRead = false;
    let branddriehoekTemperatuurRead = false;
    let branddriehoekFinished = true;
    const brandAlarm = new Audio("media/brandalarm.mp3");
    brandAlarm.volume = 0.1;
    brandAlarm.loop = true;

    // Clicked for gele hesje
    let hesjeTextFinished = false;
    let clickedForHesje = true;

    const camera = document.getElementById("js--camera");
    const camera2 = document.getElementById("js--camera2");

    const moveObject = document.getElementsByClassName("js--movement_object");
    const movement = document.getElementById("js--movementContainer");

    const apiCilinder = document.getElementById("js--apiCilinder");

    const deur = document.getElementsByClassName("js--deur");
    const deurVerplaatsBuddy = document.getElementById("js--deur2");
    const deurVoorBrand = document.getElementById("js--deur");
    const deurBrandContainer = document.getElementById("js--deurBrandContainer");

    const blusser = document.getElementById("js--blusser");

    const blusserExplained = document.getElementById("js--blusserExplained");
    const blusserPin = document.getElementById("js--blusserPin");
    const blusserHandle = document.getElementById("js--blusserHandle");
    const blusserInteraction = document.getElementById("js--blusserInteractionFocus");
    const blusserSchuim = document.getElementsByClassName("js--blusSchuim");
    const blusserContainer = document.getElementById("js--blusserContainer");
    let blusserHolding;

    const fire = document.getElementById("js--fire");

    const certificateContainer = document.getElementById("js--certificateContainer");
    const certificateTextDate = document.getElementById("js--certificateTextDate");
    const certificateTextLocation = document.getElementById("js--certificateTextLocation");

    const text = document.getElementsByClassName("js--text");
    let textVisible = false;

    const branddriehoekParts = document.getElementsByClassName("js--branddriehoekPart");
    const branddriehoekPlanes = document.getElementsByClassName("js--branddriehoekPlane");
    const branddriehoekTeksten = document.getElementsByClassName("js--branddriehoekTekst");
    const branddriehoek = document.getElementById("js--branddriehoek");
    const branddriehoekPartTemperatuur = document.getElementById("js--branddriehoekPart-temperatuur");
    const branddriehoekPartBrandstof = document.getElementById("js--branddriehoekPart-brandstof");
    const branddriehoekPartZuurstof = document.getElementById("js--branddriehoekPart-zuurstof");

    const startButton = document.getElementById("js--startButton");
    const startButtonText = document.getElementById("js--startButtonText");
    const cursorText = document.getElementById("js--cursorText");

    const buddy = document.getElementById("js--buddy");

    const buddyDialogPosition = document.getElementById("js--text-position");
    const buddyPosition = document.getElementById("js--buddy-position");

    const placeBuddy1 = document.getElementById("js--placeBuddy1");
    const placeBuddy2 = document.getElementById("js--placeBuddy2");
    const placeBuddy3 = document.getElementById("js--placeBuddy3");
    const placeBuddy4 = document.getElementById("js--placeBuddy4");

    const textIcon = document.getElementById("js--text-icon");
    const textIcon2 = document.getElementById("js--text-icon2");
    const playIcon = document.getElementById("js--play-icon");

    const test = document.getElementsByClassName("js--test");

    const leftHand = document.getElementById("js--leftHand");
    const rightHand = document.getElementById("js--rightHand");

    const brandnummerKaart = document.getElementById("js--noodnummer");


    // main code
    const openTextIcon = () => {
        textIcon.onclick = () => {
            if (textVisible) {
                textIcon.setAttribute("position", "0.75 2.2 0");
                textIcon2.setAttribute("position", "0.75 2.2 -0.02");
                for (let i = 0; i < text.length; i++) {
                    text[i].emit("becomeInvisible");
                }
                textVisible = false;
            }
            else {
                textIcon2.setAttribute("position", "0.75 2.2 0.");
                textIcon.setAttribute("position", "0.75 2.2 -0.02");
                for (let i = 0; i < text.length; i++) {
                    text[i].emit("becomeVisible");
                }
                textVisible = true;
            }
        };
    };

    deurVerplaatsBuddy.onclick = () => {
        buddy.setAttribute("position", "3 0 -21.5");
        buddy.setAttribute("rotation", "0 -90 0");

        if (deurVerplaatsBuddy.getAttribute("rotation").y == -80) {
            deurVerplaatsBuddy.setAttribute("animation", "property: rotation; easing: linear; dur: 500; to: 0 0 0");
        }
        else if (deurVerplaatsBuddy.getAttribute("rotation").y == 0) {
            deurVerplaatsBuddy.setAttribute("animation", "property: rotation; easing: linear; dur: 500; to: 0 80 0");
        }
    };

    deurVoorBrand.onclick = () => {
        if (openDeur == false) {
            deurVoorBrand.setAttribute("animation", "property: rotation; easing: linear; dur: 500; to: 0 0 0");
            openDeur = true;
        }
        else if (openDeur) {
            deurVoorBrand.setAttribute("animation", "property: rotation; easing: linear; dur: 500; to: 0 80 0");
            openDeur = false;
        }
    };

    placeBuddy1.onclick = () => {
        buddy.emit("moveToPosition1");
        buddy.emit("rotate1");
    };

    placeBuddy2.onclick = () => {
        buddy.emit("moveToPosition2");
        buddy.emit("rotate2");
    };

    placeBuddy3.onclick = () => {
        buddy.emit("moveToPosition3");
        buddy.emit("rotate3");
    };

    placeBuddy4.onclick = () => {
        buddy.emit("moveToPosition4");
        buddy.emit("rotate4");
    };

    const focusOnObject = (object) => {
        let focus = "property: scale; dir: alternate; easing: linear; loop: true; dur: 750; from: 0.8 0.8 0.8; to: "
            + (object.getAttribute("scale").x + 0.06)
            + " "
            + (object.getAttribute("scale").y + 0.06)
            + " "
            + (object.getAttribute("scale").z + 0.06);
        object.setAttribute("animation", focus);
    };

    focusOnObject(startButtonText);

    const startExperience = () => {
        startButton.onclick = () => {
            startButton.setAttribute("visible", "false");
            startButton.setAttribute("position", "0 20 -5");
            startButtonText.setAttribute("visible", "false");
            cursorText.setAttribute("visible", "false");

            dialogPlayAudioAndSetText(1, buddyDialogPosition);
            delay(dialogGetTime(1))
                .then(() => {
                    act2();
                });
        };
    };

    const act2 = () => {
        toggleClickable(playIcon);
        focusOnObject(playIcon);
        playIcon.onclick = () => {
            togglePlayIconVisibility();
            toggleClickable(playIcon);
            playIcon.setAttribute("position", "false");
            playIcon.removeAttribute("animation");
            dialogPlayAudioAndSetText(2, buddyDialogPosition);
            delay(dialogGetTime(2))
                .then(() => {
                    act3();
                });
        };
    };

    const act3 = () => {
        togglePlayIconVisibility();
        toggleClickable(playIcon);
        focusOnObject(playIcon);
        playIcon.onclick = () => {
            togglePlayIconVisibility();
            toggleClickable(playIcon);
            playIcon.setAttribute("position", "false");
            playIcon.removeAttribute("animation");
            dialogPlayAudioAndSetText(3, buddyDialogPosition);
            branddriehoekPartTemperatuur.emit("anim-movement1");
            branddriehoekPartTemperatuur.emit("anim-movement2");
            branddriehoekPartTemperatuur.emit("anim-movement3");
            delay(dialogGetTime(3))
                .then(() => {
                    act4();
                });
        };
    };

    const act4 = () => {
        togglePlayIconVisibility();
        toggleClickable(playIcon);
        focusOnObject(playIcon);
        playIcon.onclick = () => {
            togglePlayIconVisibility();
            toggleClickable(playIcon);
            playIcon.setAttribute("position", "false");
            playIcon.removeAttribute("animation");
            dialogPlayAudioAndSetText(4, buddyDialogPosition);
            branddriehoekPartBrandstof.emit("anim-movement1");
            branddriehoekPartBrandstof.emit("anim-movement2");
            branddriehoekPartBrandstof.emit("anim-movement3");
            delay(dialogGetTime(4))
                .then(() => {
                    act5();
                });
        };
    };

    const act5 = () => {
        togglePlayIconVisibility();
        toggleClickable(playIcon);
        focusOnObject(playIcon);
        playIcon.onclick = () => {
            togglePlayIconVisibility();
            toggleClickable(playIcon);
            playIcon.setAttribute("position", "false");
            playIcon.removeAttribute("animation");
            dialogPlayAudioAndSetText(5, buddyDialogPosition);
            branddriehoekPartZuurstof.emit("anim-movement1");
            branddriehoekPartZuurstof.emit("anim-movement2");
            branddriehoekPartZuurstof.emit("anim-movement3");
            delay(dialogGetTime(5))
                .then(() => {
                    act6();
                });
        };
    };

    const act6 = () => {
        togglePlayIconVisibility();
        toggleClickable(playIcon);
        focusOnObject(playIcon);
        playIcon.onclick = () => {
            togglePlayIconVisibility();
            toggleClickable(playIcon);
            playIcon.setAttribute("position", "false");
            playIcon.removeAttribute("animation");
            dialogPlayAudioAndSetText(6, buddyDialogPosition);
            delay(dialogGetTime(6))
                .then(() => {
                    act7();
                });
        };
    };

    const act7 = () => {
        togglePlayIconVisibility();
        toggleClickable(playIcon);
        focusOnObject(playIcon);
        playIcon.onclick = () => {
            brandAlarm.play();
            togglePlayIconVisibility();
            toggleClickable(playIcon);
            playIcon.setAttribute("position", "false");
            playIcon.removeAttribute("animation");
            dialogPlayAudioAndSetText(7, buddyDialogPosition);
            delay(dialogGetTime(7))
                .then(() => {
                    branddriehoek.emit("anim-driehoekLeft");
                    branddriehoek.emit("anim-driehoekRotate");
                    movement.setAttribute("animation", "property: position; easing: linear; dur: 1000; to: 0 0 0");
                    moveObject[0].emit("click");
                    act8();
                });
        };
    };

    const act8 = () => {
        placeBuddy1.onclick = () => {
            buddy.emit("moveToPosition1");
            buddy.emit("rotate1");

            if (act8HasntPlayed) {
                disableMovement();
                MovementCheck = false;


                dialogPlayAudioAndSetText(8, buddyDialogPosition);
                delay(dialogGetTime(8))
                    .then(() => {
                        act9();
                    });
                act8HasntPlayed = false;
            }

        };
    };

    const act9 = () => {

        MovementCheck = true;
        enableMovement();

        placeBuddy3.onclick = () => {

            buddy.emit("moveToPosition3");
            buddy.emit("rotate3");

            if (act9HasntPlayed) {

                disableMovement();
                MovementCheck = false;

                dialogPlayAudioAndSetText(9, buddyDialogPosition);
                delay(dialogGetTime(9))
                    .then(() => {
                        act10();
                    });
            }

            act9HasntPlayed = false;
        };
    };

    const act10 = () => {
        togglePlayIconVisibility();
        toggleClickable(playIcon);
        focusOnObject(playIcon);
        playIcon.onclick = () => {
            togglePlayIconVisibility();
            toggleClickable(playIcon);
            playIcon.setAttribute("position", "false");
            playIcon.removeAttribute("animation");
            dialogPlayAudioAndSetText(10, buddyDialogPosition);
            delay(dialogGetTime(10))
                .then(() => {
                    act11();
                });
        };
    };

    const act11 = () => {
        toggleClickable(deurVoorBrand);
        deurVoorBrand.onclick = () => {
            toggleClickable(deurVoorBrand);
            deurCheckAnimation();
            dialogPlayAudioAndSetText(11, buddyDialogPosition);
            delay(dialogGetTime(11))
                .then(() => {
                    act12();
                });
        };
    };

    const act12 = () => {
        toggleClickable(deurVoorBrand);
        deurVoorBrand.onclick = () => {
            toggleClickable(deurVoorBrand);
            deurVoorBrand.emit("deurOpKier");
            dialogPlayAudioAndSetText(12, buddyDialogPosition);
            delay(dialogGetTime(12))
                .then(() => {
                    act13();
                });
        };
    };

    const act13 = () => {

        toggleClickable(deurVoorBrand);
        deurVoorBrand.onclick = () => {
            toggleClickable(deurVoorBrand);
            deurVoorBrand.emit("deurDicht");

            MovementCheck = true;
            enableMovement();

            placeBuddy1.onclick = () => {
                buddy.emit("moveToPosition1");
                buddy.emit("rotate1");

                if (act13HasntPlayed) {
                    disableMovement();
                    MovementCheck = false;
                    dialogPlayAudioAndSetText(13, buddyDialogPosition);
                    delay(dialogGetTime(13))
                        .then(() => {
                            act14();
                        });
                    act13HasntPlayed = false;
                }
            };
        };
    };

    const act14 = () => {
        togglePlayIconVisibility();
        toggleClickable(playIcon);
        focusOnObject(playIcon);
        playIcon.onclick = () => {
            togglePlayIconVisibility();
            toggleClickable(playIcon);
            playIcon.setAttribute("position", "false");
            playIcon.removeAttribute("animation");
            dialogPlayAudioAndSetText(14, buddyDialogPosition);
            delay(dialogGetTime(14))
                .then(() => {
                    act15();
                });
        };
    };

    const act15 = () => {
        toggleClickable(blusser);
        blusser.onclick = () => {
            camera2.innerHTML += '<a-entity id="js--blusserHolding" gltf-model="#BlusserHold" position="0.5 -1 -1" rotation="0 0 0"></a-entity>';
            blusserHolding = document.getElementById("js--blusserHolding");
            blusser.remove();

            MovementCheck = true;
            enableMovement();

            placeBuddy3.onclick = () => {

                buddy.emit("moveToPosition3");
                buddy.emit("rotate3");

                if (act15HasntPlayed) {

                    disableMovement();
                    MovementCheck = false;

                    placeBuddy4.emit("becomeVisible");


                    toggleClickable(blusser);
                    dialogPlayAudioAndSetText(15, buddyDialogPosition);
                    delay(dialogGetTime(15))
                        .then(() => {
                            act16();
                        });
                }

                act15HasntPlayed = false;
            };
        };
    };

    const act16 = () => {
        toggleClickable(deurVoorBrand);
        deurVoorBrand.onclick = () => {
            deurVoorBrand.setAttribute("animation", "property: rotation; easing: linear; dur: 500; to: 0 80 0");
            toggleClickable(deurVoorBrand);

            MovementCheck = true;
            enableMovement();

            placeBuddy4.onclick = () => {

                buddy.emit("moveToPosition4");
                buddy.emit("rotate4");

                if (act16HasntPlayed) {

                    disableMovement();
                    MovementCheck = false;

                    blusserHolding.remove();
                    blusserContainer.setAttribute("visible", true);


                    dialogPlayAudioAndSetText(16, buddyDialogPosition);
                    delay(dialogGetTime(16))
                        .then(() => {
                            act17();
                        });
                }

                act16HasntPlayed = false;
            };
        };
    };

    const act17 = () => {
        toggleClickable(blusserInteraction);
        focusOnObject(blusserInteraction);
        blusserInteraction.onclick = () => {
            blusserAnimationPin();
            blusserAnimationTest();
            setTimeout(function () {
                toggleClickable(blusserInteraction);
                blusserInteraction.removeAttribute("animation");
                dialogPlayAudioAndSetText(17, buddyDialogPosition);
                delay(dialogGetTime(17))
                    .then(() => {
                        act18();
                    });
            }, 3000);

        };
    };

    const act18 = () => {
        toggleClickable(blusserInteraction);
        focusOnObject(blusserInteraction);
        blusserInteraction.onclick = () => {

            blusserAnimationBlussen();

            setTimeout(function () {

                toggleClickable(blusserInteraction);


                brandnummerKaart.emit("turnVisible");


                blusserInteraction.removeAttribute("animation");
                dialogPlayAudioAndSetText(18, buddyDialogPosition);
                delay(dialogGetTime(18))
                    .then(() => {
                        act19();
                    });
            }, 7000);
        };
    };

    const act19 = () => {
        togglePlayIconVisibility();
        toggleClickable(playIcon);
        focusOnObject(playIcon);
        playIcon.onclick = () => {
            togglePlayIconVisibility();
            toggleClickable(playIcon);
            playIcon.setAttribute("position", "false");
            playIcon.removeAttribute("animation");
            dialogPlayAudioAndSetText(19, buddyDialogPosition);
            delay(dialogGetTime(19))
                .then(() => {
                    act20();
                });
        };
    };

    const act20 = () => {
        MovementCheck = true;
        enableMovement();

        placeBuddy3.setAttribute("position", "-11.5 0.2 -21.5");
        placeBuddy3.emit("becomeVisible");

        setApiCilinder();

        placeBuddy3.onclick = () => {

            if (act20HasntPlayed) {

                activateCertificate();
                disableMovement();
                MovementCheck = false;
                placeBuddy4.emit("becomeInvisible")

                dialogPlayAudioAndSetText(20, buddyDialogPosition);
                delay(dialogGetTime(20))
                    .then(() => {
                        showAPI();
                        makeAPIInteractive();
                    });
            }

            act20HasntPlayed = false;
        };
    };

    const act21 = () => {
        dialogPlayAudioAndSetText(21, buddyDialogPosition);
    };
    // ------------------ Movement ------------------

    const setMovementObjects = () => {
        for (let i = 0; i < moveObject.length; i++) {
            moveObject[i].setAttribute("class", "js--movement_object");
            moveObject[i].addEventListener('click', function (event) {
                let moveSpeed = 1000;
                camera.setAttribute("animation", "property: position; easing: linear; dur: " + moveSpeed + "; to: " + this.getAttribute("position").x + " 0 " + this.getAttribute("position").z);
            });
        }
        moveObject[1].setAttribute("class", "js--movement_object clickable");
    };

    const dyeCilindersGreen = () => {
        for (let i = 0; i < moveObject.length; i++) {
            moveObject[i].emit("becomeInvisible");
            if (moveObject[i].classList.contains("clickable")) {
                moveObject[i].classList.remove("clickable");
            }
        }
    };

    let MovementCheck = true;

    const checkCurrentMovementObject = () => {

        for (let i = 0; i < moveObject.length; i++) {
            moveObject[i].addEventListener('click', function (event) {
                if (i == 0 && MovementCheck) {
                    dyeCilindersGreen();
                    moveObject[i].emit("becomeVisible");
                    moveObject[i + 1].emit("becomeVisible");

                    toggleClickable(moveObject[i + 1]);
                }
                else if (i == moveObject.length - 1 && MovementCheck) {
                    dyeCilindersGreen();
                    moveObject[i].emit("becomeVisible");
                    moveObject[i - 1].emit("becomeVisible");

                    toggleClickable(moveObject[i + 1]);
                }
                else if (MovementCheck) {
                    dyeCilindersGreen();
                    moveObject[i].emit("becomeVisible");
                    moveObject[i + 1].emit("becomeVisible");
                    moveObject[i - 1].emit("becomeVisible");

                    toggleClickable(moveObject[i - 1]);
                    toggleClickable(moveObject[i + 1]);
                }
            });
        }
    };

    const disableMovement = () => {
        for (let i = 0; i < moveObject.length; i++) {
            moveObject[i].setAttribute("class", "js--movement_object");
            moveObject[i].emit("becomeInvisible");
        }
    }

    const enableMovement = () => {
        for (let i = 0; i < moveObject.length; i++) {
            moveObject[i].setAttribute("class", "js--movement_object clickable");
        }
    }

    let enableApiCilinder = false;

    const setApiCilinder = () => {
        apiCilinder.emit("becomeVisible");
        apiCilinder.setAttribute("class", "clickable");
        enableApiCilinder = true;

        apiCilinder.onclick = () => {
            if (enableApiCilinder) {
                camera.setAttribute("animation", "property: position; easing: linear; dur: 1000; to: -13.5 0.2 -21.5");
                act21();
            }
        }
    }

    // ------------------ DeurCheck -----------------

    const deurCheckAnimation = () => {
        leftHand.emit("handOpacityIn");
        rightHand.emit("handOpacityIn");

        setTimeout(function () {
            leftHand.emit("leftHandBoven");
            rightHand.emit("rightHandBoven")
        }, 2000);

        setTimeout(function () {
            leftHand.emit("leftHandBeneden");
            rightHand.emit("rightHandBeneden");
        }, 4000);

        setTimeout(function () {
            rightHand.emit("rightHandKlink");
        }, 6500);

        setTimeout(function () {
            leftHand.emit("handOpacityOut");
            rightHand.emit("handOpacityOut");
        }, 8000);

    };

    // --------------- BlusserAnimation -------------

    const blusserAnimationPin = () => {
        blusserPin.emit("pinOut");
        blusserInteraction.emit("pinFadeOut");
        setTimeout(function () {
            blusserInteraction.setAttribute("position", "0.103 0.951 -0.009");
            blusserInteraction.setAttribute("rotation", "-5 90 0");
            blusserInteraction.emit("pinFadeIn");
        }, 600);
    }

    const blusserAnimationTest = () => {
        setTimeout(function () {
            blusserHandle.emit("handleDown");
            blusserInteraction.emit("pinFadeOut");
            blusserAnimationStoot();
        }, 1000)
        setTimeout(function () {
            blusserHandle.emit("handleUp");
            blusserInteraction.emit("pinFadeIn");
        }, 2000);
    }

    const blusserAnimationStoot = () => {
        for (let i = 0; i < blusserSchuim.length; i++) {
            blusserSchuim[i].setAttribute("visible", true);
            blusserSchuim[i].emit("schuimPosition");
            blusserSchuim[i].emit("schuimScale");
            blusserHandle.emit("handleDown");
            setTimeout(function () {
                blusserSchuim[i].setAttribute("visible", false);
                blusserInteraction.emit("pinFadeIn");
                blusserHandle.emit("handleUp");
            }, 500);
        }
    }

    const blusserAnimationBlussen = () => {

        for (let i = 0; i < blusserSchuim.length; i++) {
            blusserSchuim[i].setAttribute("visible", true);
            blusserInteraction.emit("pinFadeOut");
            blusserHandle.emit("handleDown");

            setTimeout(function () {
                blusserSchuim[i].setAttribute("visible", false);
                removeFire();
                blusserHandle.emit("handleUp");
            }, 6000);
            setTimeout(function () {
                removeBlusser();
            }, 7000);
        }
    }

    let blusserAnimationStep = true;
    let blusserAnimationCheck = true;

    blusserInteraction.onclick = () => {
        if (blusserAnimationStep && blusserAnimationCheck) {
            blusserAnimationPin();
            blusserAnimationStep = false;
        }
        else if (!blusserAnimationStep && blusserAnimationCheck) {
            blusserAnimationTest();
            blusserAnimationStep = true;
            blusserAnimationCheck = false;
        }

        else if (!blusserAnimationCheck && !blusserAnimationCheck) {
            blusserAnimationBlussen();
        }
    }

    const setBranddriehoek = () => {
        for (let i = 0; i < branddriehoekParts.length; i++) {

            branddriehoekParts[i].onmouseenter = () => {
                branddriehoek.removeAttribute("animation");
            };

            branddriehoekParts[i].onmouseleave = () => {
                switch (i) {
                    case 0:
                        branddriehoekZuurstofRead = true;

                        break;
                    case 1:
                        branddriehoekTemperatuurRead = true;

                        break;
                    case 2:
                        branddriehoekBrandstofRead = true;

                        break;
                }
                readEntireBranddriehoek();

            };
        }
    };

    const setDeuren = () => {

        for (let i = 0; i < deur.length; i++) {
            deur[i].addEventListener('click', function (event) {
                if (deur[i].getAttribute("rotation").y == 80) {
                    deur[i].setAttribute("animation", "property: rotation; easing: linear; dur: 500; to: 0 0 0");
                }
                else {
                    deur[i].setAttribute("animation", "property: rotation; easing: linear; dur: 500; to: 0 80 0");
                }
            });
        }
    };

    const activateCertificate = () => {
        certificateContainer.emit("turnVisible");
        certificateTextDate.emit("turnVisible");
        certificateTextLocation.emit("turnVisible");
    };

    const showAPI = () => {
        for (let i = 0; i < years_api.length; i++) {
            years_api[i].setAttribute("opacity", "0.5");
            yearsText_api[i].setAttribute("opacity", "0.5");
        }
        text_api.setAttribute("opacity", "1");
        textYear_api.setAttribute("opacity", "1");
    };

    const hideAPI = () => {
        for (let i = 0; i < years_api.length; i++) {
            years_api[i].setAttribute("opacity", "0");
            yearsText_api[i].setAttribute("opacity", "0");
            bordersOfText_api[i].setAttribute("opacity", "0");
        }
        textYear_api.setAttribute("opacity", "0");
        text_api.setAttribute("opacity", "0");
    };

    const setLaptop = () => {
        laptop.onclick = (event) => {
            if (hold == true) {
                fire.remove();
                blusser2.remove();
                buddyPosition.removeAttribute("sound");
                hold = false;
            }
        };
    };

    const removeFire = () => {
        fire.setAttribute("visible", false);
    };

    const removeBlusser = () => {
        blusserContainer.setAttribute("visible", false);
        buddyPosition.removeAttribute("sound");
        certificateContainer.emit("turnVisible");
        certificateTextDate.emit("turnVisible");
        certificateTextLocation.emit("turnVisible");
    }

    const typeEffectPosition = (element, speed) => {
        buddyDialogPosition.setAttribute("value", "");
        let i = 0;
        let timer = setInterval(function () {
            if (i < element.length) {
                buddyDialogPosition.setAttribute("value", buddyDialogPosition.getAttribute("value") + element.charAt(i));
                i++;
            } else {
                clearInterval(timer);
            }
        }, speed);
    };

    const readEntireBranddriehoek = () => {

        if (branddriehoekBrandstofRead && branddriehoekTemperatuurRead && branddriehoekZuurstofRead && branddriehoekFinished) {
            branddriehoekFinished = false;


            talk = "Bij een brand in dit gebouw is het van belang dat jij als toekomstig BHV'er gelijk weet te handelen. Stap 1 is in contact komen met de coordinator BHV. Hij geeft je de instructies die je moet ondernemen. Ook ontvang je een gele hesje. Klik op mij!";
            typeEffectPosition(talk, speed);

            setTimeout(function () {
                buddyPosition.onclick = () => {
                    geleHesjeText();
                };
            }, speed * talk.length);
            booleaan = true;
        }
    };

    const geleHesjeText = () => {
        if (clickedForHesje) {
            clickedForHesje = false;
            const sound = document.createAttribute("sound");
            sound.value = "src: url(media/brandalarm.mp3); autoplay: true; loop: true";
            buddyPosition.setAttribute("sound", sound.value);
            talk = "Het gele hesje heb je nu in zit. O NEE HE... HET BRAND ALARM GAAT AF. Dit kan geen toeval zijn. Laat mij jouw als BHV-coordinator helpen de instructies te geven om deze brand zo snel mogelijk te stoppen. Ga snel naar de locatie van de brand! Volg de groene cilinders.";
            typeEffectPosition(talk, speed);
            booleaan = false;

            setTimeout(function () {
                setMovementObjects();
                placeBuddy4.setAttribute("class", "js--movement_object");
                movement.setAttribute("animation", "property: position; easing: linear; dur: 1000; to: 0 0 0");
            }, 8000);
        }
    };

    const activateDateCertificaat = () => {
        const certificateTextDate = document.getElementById("js--certificateTextDate");

        let today = new Date();
        let dd = String(today.getDate()).padStart(2, '0');
        let mm = String(today.getMonth());
        let yyyy = today.getFullYear();
        let months = ["jan", "feb", "maa", "apr", "mei", "jun", "jul", "aug", "sep", "okt", "nov", "dec"];

        today = dd + " " + months[parseInt(mm)] + " " + yyyy;

        certificateTextDate.setAttribute("value", today);
    };

    // API
    let listKey = [];
    let listTitle = [];
    let totaalAlarmeringenVanDeBrandweer = { "2013": 0, "2014": 0, "2015": 0, "2016": 0, "2017": 0, "2018": 0, "2019": 0 };

    const years_api = document.getElementsByClassName("js--yearBox");
    const yearsText_api = document.getElementsByClassName("js--yearBoxText");
    const textYear_api = document.getElementById("js--textYear");
    const bordersOfText_api = document.getElementsByClassName("js--yearBoxBorder");
    const text_api = document.getElementById("js--textAPI");

    const BASEURL = "https://opendata.cbs.nl/ODataApi/OData/83122NED/";

    const fillKeysAndTitles = () => {
        fetch(BASEURL + "RegioS")
            .then((data) => {
                return data.json();
            })
            .then((response) => {
                for (let i = 0; i < response.value.length; i++) {
                    let temp = response.value[i].Title;

                    if (temp.includes("Zuid-Holland")) {
                        listKey.push(response.value[i].Key);
                        listTitle.push(response.value[i].Title);
                    }
                }
            });
    };

    const fillTotaalAlarmeringenVanDeBrandweer = () => {
        fetch(BASEURL + "UntypedDataSet")
            .then((data) => {
                return data.json();
            })
            .then((response) => {
                for (let i = 0; i < response.value.length; i++) {
                    if (listKey.includes(response.value[i].RegioS)) {
                        totaalAlarmeringenVanDeBrandweer[response.value[i].Perioden.substring(0, 4)] += parseInt(response.value[i].TotaalAlarmeringenVanDeBrandweer_1);
                    }
                }
            });
    };

    for (let i = 0; i < years_api.length; i++) {
        years_api[i].onclick = () => {
            for (let i = 0; i < years_api.length; i++) {
                years_api[i].emit("becomeInvisibleHalf");
                yearsText_api[i].emit("becomeInvisibleHalf");
                bordersOfText_api[i].emit("becomeInvisible");
            }
            years_api[i].emit("becomeVisible");
            yearsText_api[i].emit("becomeVisible");
            bordersOfText_api[i].emit("becomeVisible");
            let length = String(totaalAlarmeringenVanDeBrandweer[yearsText_api[i].getAttribute("value")]).length;
            textYear_api.setAttribute("value", String(totaalAlarmeringenVanDeBrandweer[yearsText_api[i].getAttribute("value")]).slice(0, 2)
                + "." +
                String(totaalAlarmeringenVanDeBrandweer[yearsText_api[i].getAttribute("value")]).slice(2, length)
                + " keer uitgerukt.");
        };
    }

    const makeAPIInteractive = () => {
        for (let i = 0; i < years_api.length; i++) {
            toggleClickable(years_api[i]);
        }
    }

    //Function calls
    openTextIcon();
    startExperience();
    setDeuren();
    setLaptop();
    fillKeysAndTitles();
    fillTotaalAlarmeringenVanDeBrandweer();
    checkCurrentMovementObject();
    activateDateCertificaat();
    setMovementObjects();
    hideAPI();
};
