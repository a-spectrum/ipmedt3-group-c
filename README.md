# Ipmedt3 group C

https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet

Command | Explanation
--- | ---
clear |
git log |
git branch --all |
git checkout [branch or commit name]  | (Switch to existing branch or commit, throws error if it doesn't exist)  
git checkout -b [branch name]         | (Creates new branch)  
git add .                             | (Stage files in preparation of commit)  
git commit -m "Describe commit here"  | (Creates a save point (Photo in Jeroen's explanation) of your work locally that you could revert back to.)  
git merge [branch name]               | (Merges the named branch into your current branch)  
git push                              | (Push current local branch to repo)  
git push -u origin [local branch]     | (Creates a copy of your local branch inside the repo so it is also tracked)    
git fetch                             | (Check for changes in repo compared to your local version)  
git pull                              | (Download all changes in the repo to your local version)  
